package com.shymoniak.model;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SmsSender {
    public static final String ACCOUNT_SID = "AC525932337267629185f3f5e8afb9635e";
    public static final String AUTH_TOKEN = "ac7ec5a5b0c55239c24cc681016af429";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        Message message = Message.creator(new PhoneNumber("+380935124640"),
                new PhoneNumber("+12052739165"), str).create();
    }
}
