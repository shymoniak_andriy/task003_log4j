package com.shymoniak;

import org.apache.logging.log4j.*;

public class App {
    private static Logger LOGGER = LogManager.getLogger(App.class);
    public static void main(String[] args) {
        System.out.println("Hello World");
        LOGGER.trace("This is a trace message");
        LOGGER.debug("This is a debug message");
        LOGGER.info("This is an info message");
        LOGGER.warn("This is a warn message");
        LOGGER.error("This is an error message");
        LOGGER.fatal("This is a fatal message");
    }
}
